<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\SearchPlugin\Controller;

use JMS\Serializer\SerializationContext;
use Omni\Sylius\SearchPlugin\Event\SearchPluginEvents;
use Omni\Sylius\SearchPlugin\Event\SearchResultsEvent;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SearchController extends Controller
{
    /**
     * @param Request $request
     *
     * @return Response
     */
    public function searchAction(Request $request)
    {
        $pager = $this->get('omni_search.finder')->getPager($request);
        $results = $this->get('omni_search.finder')->find($pager->getCurrentPageResults());

        $event = new SearchResultsEvent($results);

        $this->get('event_dispatcher')->dispatch(SearchPluginEvents::SEARCH_RESULTS, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $data = [
            'results' => $results,
            'searchTerm' => $request->get('q'),
        ];

        if ($request->isXmlHttpRequest()) {
            return new JsonResponse(
                $this->serializeData($data),
                Response::HTTP_OK,
                [],
                true
            );
        }

        $data['pager'] = $pager;

        return $this->render('OmniSyliusSearchPlugin::search_results.html.twig', $data);
    }

    /**
     * @param array $data
     *
     * @return string
     */
    private function serializeData(array $data): string
    {
        $serializationContext = SerializationContext::create()->setGroups(
            $this->getParameter('omni_search.serialization_groups')
        );

        return $this->get('jms_serializer')->serialize($data, 'json', $serializationContext);
    }
}
