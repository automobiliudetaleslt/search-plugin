<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\SearchPlugin\Listener;

use Omni\Sylius\SearchPlugin\Indexer\Indexer;
use Sylius\Bundle\ResourceBundle\Event\ResourceControllerEvent;

class ResourceListener
{
    /**
     * @var Indexer
     */
    private $indexer;

    /**
     * @param Indexer $indexer
     */
    public function __construct(Indexer $indexer)
    {
        $this->indexer = $indexer;
    }

    /**
     * @param ResourceControllerEvent $event
     */
    public function createResourceIndex(ResourceControllerEvent $event)
    {
        $this->indexer->createIndexForResource($event->getSubject());
    }

    /**
     * @param ResourceControllerEvent $event
     */
    public function onRemove(ResourceControllerEvent $event)
    {
        $this->indexer->removeIndexForResource($event->getSubject());
    }
}
